const path = require('path');

module.exports = {
    entry: './src/ome/ome.js',
    mode: 'development',
    module: {
        rules: [
            {
                test: /\.m?js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                }
            },
        ],
    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js']
    },
    output: {
        filename: 'ol3-viewer.js',
        path: path.resolve(__dirname, 'dist')
    }
};
